from django.contrib.auth import get_user_model

import factory

from image.models import ImageCollection, Image
from user.models import AccountTier


User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker("name")


class AccountTierFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AccountTier

    user = factory.SubFactory(UserFactory)
    name = "testingAccountTier"
    link_to_original = True
    expiring_links = True


class ImageCollectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ImageCollection

    user = factory.SubFactory(UserFactory)
    counter = 1


class ImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Image

    image_collection = factory.SubFactory(ImageCollectionFactory)
