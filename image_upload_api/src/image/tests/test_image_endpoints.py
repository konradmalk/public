import os

from django.conf import settings
from rest_framework.test import APITestCase
from rest_framework import status

from unittest.mock import patch, Mock

from image.tests.factories import UserFactory, ImageCollectionFactory, ImageFactory, AccountTierFactory


class ImageViewSetTests(APITestCase):
    def setUp(self):
        self.user = UserFactory()
        self.client.force_authenticate(user=self.user)
        self.image_collection = ImageCollectionFactory(user=self.user)
        self.image = ImageFactory(image_collection=self.image_collection)
        self.account_tier = AccountTierFactory(user=self.user)
        self.user.account_tier = self.account_tier

    @patch('image.viewsets.get_storage_client')
    def test_upload_image(self, mock_get_storage_client):
        mock_storage_client = Mock()
        mock_get_storage_client.return_value = mock_storage_client

        mock_blob = Mock()
        mock_blob.upload_from_file.return_value = None
        mock_storage_client.bucket.return_value.blob.return_value = mock_blob

        image_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_image.png')
        with open(image_path, 'rb') as image_file:
            response = self.client.post('/api/image/', {'image': image_file}, format='multipart')

        print(response.content)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_upload_invalid_image(self):
        invalid_image_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_image_invalid.bmp')
        with open(invalid_image_path, 'rb') as invalid_image_file:
            response = self.client.post('/api/image/', {'image': invalid_image_file}, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ExpiringLinkViewSetTests(APITestCase):
    def setUp(self):
        self.user = UserFactory()
        self.client.force_authenticate(user=self.user)
        self.image_collection = ImageCollectionFactory(user=self.user)
        self.image = ImageFactory(image_collection=self.image_collection)
        self.account_tier = AccountTierFactory(user=self.user)

    @patch('image.viewsets.get_storage_client')
    def test_generate_expiring_link(self, mock_get_storage_client):
        mock_storage_client = Mock()
        mock_get_storage_client.return_value = mock_storage_client

        mock_blob = Mock()
        mock_blob.generate_signed_url.return_value = 'mock_signed_url'
        mock_storage_client.bucket.return_value.blob.return_value = mock_blob

        response = self.client.post('/api/image/expiring_link/', {'id': self.image.id, 'time': 300})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('expiring_link', response.data)

        mock_get_storage_client.assert_called_once()
        bucket_name = settings.GS_BUCKET_NAME
        mock_storage_client.bucket.assert_called_once_with(bucket_name)
        mock_blob.generate_signed_url.assert_called_once_with(expiration=300, version='v4')

    @patch('image.viewsets.get_storage_client')
    def test_generate_expiring_link_invalid_id(self, mock_get_storage_client):
        mock_storage_client = Mock()
        mock_get_storage_client.return_value = mock_storage_client

        mock_blob = Mock()
        mock_blob.generate_signed_url.return_value = 'mock_signed_url'
        mock_storage_client.bucket.return_value.blob.return_value = mock_blob

        response = self.client.post('/api/image/expiring_link/', {'id': 999999, 'time': 300})

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertNotIn('expiring_link', response.data)

    @patch('image.viewsets.get_storage_client')
    def test_generate_expiring_link_invalid_time(self, mock_get_storage_client):
        mock_storage_client = Mock()
        mock_get_storage_client.return_value = mock_storage_client

        mock_blob = Mock()
        mock_blob.generate_signed_url.return_value = 'mock_signed_url'
        mock_storage_client.bucket.return_value.blob.return_value = mock_blob

        response = self.client.post('/api/image/expiring_link/', {'id': self.image.id, 'time': 10})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn('expiring_link', response.data)
