from django.contrib import admin
from image.models import ImageCollection

admin.site.register(ImageCollection)
