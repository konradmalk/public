from django.contrib.auth.models import AbstractUser
from django.db import models


class AccountTier(models.Model):
    name = models.TextField(max_length=200)
    link_to_original = models.BooleanField(default=False)
    expiring_links = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class AllowedHeight(models.Model):
    height = models.PositiveIntegerField()
    tier = models.ForeignKey(AccountTier, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.tier}: {self.height}px"


class User(AbstractUser):
    account_tier = models.ForeignKey(AccountTier, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.username
