from django.contrib import admin

from user.models import User, AccountTier, AllowedHeight

admin.site.register(User)
admin.site.register(AccountTier)
admin.site.register(AllowedHeight)
