from typing import List, Tuple, Optional
from assets.actors import Hero


class Tile:
    x: int
    y: int
    hero: Optional[Hero] = None
    enemies: List = []

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __str__(self):
        if self.hero:
            if self.enemies:
                return "X"
            return "H"
        if self.enemies:
            return "E"
        return "_"


class Map:
    grid: List[List]
    hero_position: Optional[Tuple[int, int]] = None

    def __init__(self, x, y, hero: Hero):
        self.grid = [[] for _ in range(x)]
        for i in range(x):
            for j in range(y):
                self.grid[i].append(Tile(x=i, y=j))
        self.move_hero(new_x=x - 1, new_y=y - 1, hero=hero)

    def move_hero(self, new_x, new_y, hero=None):
        if self.hero_position:
            old_x, old_y = self.hero_position
            hero = self.grid[old_x][old_y].hero
            self.grid[old_x][old_y].hero = None
        self.grid[new_x][new_y].hero = hero
        self.hero_position = new_x, new_y

    def __str__(self):
        result = ""
        for row in self.grid:
            for tile in row:
                result += str(tile)
            result += "\n"
        return result
