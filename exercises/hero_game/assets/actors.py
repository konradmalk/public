from typing import Optional

from assets.items import Armor, Weapon


class Actor:
    hp = 1
    strength = 1
    intelligence = 1
    dexterity = 1
    base_armor = 1
    weapon: Optional[Weapon] = None
    armor: Optional[Armor] = None

    @property
    def total_armor(self) -> int:
        result = self.base_armor
        if self.armor:
            result += self.armor.value
            result += min(self.dexterity, self.armor.max_dex_bonus)
        return result

    def attack(self, other):
        result = self.strength
        if self.weapon:
            result += self.weapon.value
            result += min(self.strength, self.weapon.max_str_bonus)
        other.hp -= max(result - other.total_armor, 0)


class Enemy(Actor):
    xp: int

    def __init__(self, xp: int):
        self.xp: int = xp


class Hero(Actor):
    pass
