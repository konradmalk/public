class Item:
    name: str
    value: int

    def __init__(self, name: str, value: int = 1):
        self.name: str = name
        self.value = value


class Weapon(Item):
    max_str_bonus: int

    def __init__(self, name: str, value: int, max_str_bonus: int = 1):
        super().__init__(name=name, value=value)
        self.max_str_bonus = max_str_bonus


class Armor(Item):
    max_dex_bonus: int

    def __init__(self, name: str, value: int, max_dex_bonus: int = 1):
        super().__init__(name=name, value=value)
        self.max_dex_bonus = max_dex_bonus
