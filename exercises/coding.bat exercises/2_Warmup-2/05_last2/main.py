"""Given a string, return the count of the number of times that a substring length 2 appears in the string
and also as the last 2 chars of the string, so "hixxxhi" yields 1 (we won't count the end substring).

last2('hixxhi') → 1
last2('xaxxaxaxx') → 1
last2('axxxaaxx') → 2
"""


def last2(str):
    last = str[len(str) - 2:len(str)]
    count = 0
    for i in range(0, len(str) - 2):
        if last == str[i:i + 2]:
            count += 1
    return count


assert last2('hixxhi') == 1
assert last2('xaxxaxaxx') == 1
assert last2('axxxaaxx') == 2
