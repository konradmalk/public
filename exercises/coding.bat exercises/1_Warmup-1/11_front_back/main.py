"""Given a string, return a new string where the first and last chars have been exchanged.

front_back('code') → 'eodc'
front_back('a') → 'a'
front_back('ab') → 'ba'
"""


def front_back(str):
    if len(str) == 2:
        return str[1] + str[0]
    if len(str) == 1:
        return str
    if len(str) == 0:
        return ""
    mid = str[1:len(str) - 1]
    result = str[-1] + mid + str[0]
    return result


assert front_back('code') == 'eodc'
assert front_back('a') == 'a'
assert front_back('ab') == 'ba'
