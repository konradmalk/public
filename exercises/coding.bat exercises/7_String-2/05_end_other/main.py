"""Given two strings, return True if either of the strings appears at the very end of the other string,
ignoring upper/lower case differences (in other words, the computation should not be "case sensitive").
Note: s.lower() returns the lowercase version of a string.

end_other('Hiabc', 'abc') → True
end_other('AbC', 'HiaBc') → True
end_other('abc', 'abXabc') → True
"""


def end_other(a, b):
    if a == b:
        return True
    if len(a) > len(b):
        shorter = b.lower()
        longer = a.lower()
    else:
        shorter = a.lower()
        longer = b.lower()
    length = len(shorter)
    if shorter == longer[-length:]:
        return True
    return False


assert end_other('Hiabc', 'abc') is True
assert end_other('AbC', 'HiaBc') is True
assert end_other('abc', 'abXabc') is True
