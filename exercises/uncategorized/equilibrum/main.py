def solution(A):

    for i in range(0, len(A)):
        else_left_value = 0
        else_right_value = 0
        if i == 0:
            else_left_value = 0
        else:
            else_left_value = sum(A[0:i])
        if i == len(A):
            else_right_value = 0
        else:
            else_right_value = sum(A[i+1:(len(A))])
        print("L: " + str(else_left_value) + " R: " + str(else_right_value))
        if else_left_value == else_right_value:
            return i
    return -1


x = [1, 2, 3, 0, 1, 2, 3]
y = [1, 1, 1, 1, 0, 1, 3]

assert solution(x) == 3
assert solution(y) == 4
