def counter(input):
    list = []
    list = (input.split())
    count = len(list)
    combined = ""
    for i in range(0, count):
        combined += str(list[i])
    ALPHANUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    alphanumeric_dict = {}
    for i in range(0, len(ALPHANUMERIC)):
        alphanumeric_dict[ALPHANUMERIC[i]] = 0
    for i in range(0, len(ALPHANUMERIC)):
        # print(str(alphanumeric[i]) + str(combined.count(alphanumeric[i])))
        alphanumeric_dict[ALPHANUMERIC[i]] = combined.count(ALPHANUMERIC[i])
    for key, value in dict(alphanumeric_dict).items():
        if value == 0:
            del alphanumeric_dict[key]
    return(alphanumeric_dict)
