# Char counter
Exercise:
Create counter of characters in given text. Whitespaces have to omitted (spaces, tabulators, new lines, etc.).
The function "counter" must take input string and return a dictionary where every character will have its number of occurances counted.

## counter.py
Function takes the input, slicesgit  its parts and saves them to the "list" list.
Then elements of it are added to the "combined" var, as string.
Then it creates a dictionary "alphanumeric_dict" with keys made from all the single characters given in "ALPHANUMERIC" value.
Then, occurances of every key in "combined" are counted and saved as the value of the respective key.
Function deletes the pairs in the dictionary with "0" value.
Function returns the dictionary.


## counter_v2.py
The same as counter.py, with one change:
The function will turn every biggercase character into lowercase, and count both scenarios as one occurence.