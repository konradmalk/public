def bi_to_dec(string):
    used = set(string)
    if len(used) > 2 or (len(used) == 2 and ("1" not in used or "0" not in used)):
        return "Please input a binary number."
    multiplier = 1
    result = 0
    for _ in range(1, len(string) + 1):
        result += (int(string[-_]) * multiplier)
        multiplier *= 2
    return result
