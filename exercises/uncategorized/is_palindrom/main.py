def is_palindrom(word):
    return word == word[::-1]


print(is_palindrom("andna"))

assert is_palindrom("aba") is True
assert not is_palindrom("abc") is True
assert is_palindrom("abba") is True
assert is_palindrom("abcba") is True
assert is_palindrom("1111") is True
assert is_palindrom("31213") is True
assert is_palindrom("abcdddcba") is True
