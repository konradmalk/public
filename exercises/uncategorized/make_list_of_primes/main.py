def make_list_of_primes(n):
    result = []
    for divided in range(2, n+1):
        for divider in range(2, divided+1):
            x = divided % divider
            if divided != divider and x == 0:
                break
            if divided == divider and x == 0:
                result.append(divided)
    return result


print(make_list_of_primes(100))
