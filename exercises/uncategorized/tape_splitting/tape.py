def solution(A):
    print(f"Tape can be split in {len(A)-1} pieces.")
    result = set()
    for i in range(1, len(A)):
        left = sum(A[0:i])
        right = sum(A[i:len(A)])
        diff = abs(left - right)
        print(f"Split no. {i}: \tLeft: {left} \tRight: {right} \tDifference:{diff}")
        result.add(diff)
    print(f"Result: {min(result)}")
    return min(result)


x = [3, 1, 2, 4, 3]
y = [2, 2, 3, 2, 2]
assert solution(x) == 1
assert solution(y) == 3
