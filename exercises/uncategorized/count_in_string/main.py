

def how_many_in_word(word, char):
    x = 0
    for letter in word:
        if letter == char:
            x += 1
    return x


print(how_many_in_word("pancakes", "a"))

assert how_many_in_word("pancakes", "a") == 2
assert how_many_in_word("house", "h") == 1
assert how_many_in_word("Mom", "M") == 1
