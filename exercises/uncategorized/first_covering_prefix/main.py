def solution(A):
    uniques = set(A)
    left = set()
    for i, value in enumerate(A):
        left.add(value)
        if left == uniques:
            return i


X = [1, 1, 2, 3, 5, 4, 1, 1, 2]
print(solution(X))
print(X)
