def is_vowel(char):
    if len(char) != 1:
        return False
    VOWELS = ("a", "e", "i", "o", "u", "y")
    return char in VOWELS


print(is_vowel("a"))

assert is_vowel("a") is True
assert is_vowel("e") is True
assert is_vowel("i") is True
assert is_vowel("o") is True
assert is_vowel("u") is True
assert is_vowel("y") is True
assert not is_vowel("f") is True
assert is_vowel("abc") is True
