

def prima(input):
    witnesses = 0
    dict = {}

    for i in range(0, len(input)):
        witnesses = witnesses + int(input[i])
        dict[i] = input[i]

    clapping = 0

    for i in range(0, len(input)):
        if clapping >= i:
            clapping = clapping + int(dict[i])

    if witnesses == clapping:
        return 0

    clapping = 0
    needed = 0
    for i in range(0, 999999):
        dict[0] = int(dict[0]) + 1
        needed = needed + 1
        witnesses = witnesses + 1
        for i in range(0, len(input)):
            if clapping >= i:
                clapping = clapping + int(dict[i])
            if witnesses == clapping:
                return needed


assert prima("11111") == 0
assert prima("09") == 1
assert prima("110011") == 2
assert prima("1") == 0
