def check_tuples(a, b):
    if set(b).issubset(a):
        print("First tuple contains all the elements of the second tuple.")
    if len(set(a)) == len(a):
        print("First tuple contains only unique values.")
    if len(set(b)) == len(b):
        print("Second tuple contains only unique values.")


check_tuples((1, 2, 3), (2, 3, 1, 9))
