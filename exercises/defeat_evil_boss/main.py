from random import randint
"""Create name for the evil boss"""
list_a = ["Evil", "Mighty", "Despotic", "Demonic", "Diabolic", "Supreme", "Total"]
list_n = ["Overlord", "Chancellor", "Commander", "Sorcerer", "Emperor", "Master", "Boss", "Chief"]
list_of = ["of Destruction", "of Evil", "of Hell", "the Destroyer"]

if randint(1, 2) == 1:
    b_name = f"{list_a[randint(0, len(list_a) - 1)]} {list_n[randint(0, len(list_n) - 1)]}"
else:
    b_name = f"{list_n[randint(0, len(list_n) - 1)]} {list_of[randint(0, len(list_of) - 1)]}"

"""Welcome message and description of rules"""
print(f"\n#########################################################################################"
      f"\nWelcome, hero!\n"
      f"It is time to defeat {b_name} for all the mayhem he has caused in the realm.\n"
      f"Good luck!"
      f"\n\nPossible rolls: Attack: 1-20\tHeal: 1-10 + 1-10\tRecharge: 1-5 + 1-5 + 1-5"
      f"\nHealing spends mana equal to restored health. Recharge replenishes mana."
      f"\n#########################################################################################")

"""Defining initial and max values for health and mana for both actors"""
p_health = 100
P_HEALTH_MAX = 100
p_mana = 75
P_MANA_MAX = 75
b_health = 100
B_HEALTH_MAX = 100
b_mana = 75
B_MANA_MAX = 75

while p_health > 0 and b_health > 0:
    """Ensure that the actors have no more than max stats - possible from healing and recharge actions."""
    if p_health > P_HEALTH_MAX:
        p_health = P_HEALTH_MAX
    if b_health > B_HEALTH_MAX:
        b_health = B_HEALTH_MAX
    if p_mana > P_MANA_MAX:
        p_mana = P_MANA_MAX
    if b_mana > B_MANA_MAX:
        b_mana = B_MANA_MAX
    """Print status of both actors."""
    print(f"\nPlayer: \tHealth: {p_health}/{P_HEALTH_MAX}\t Mana: {p_mana}/{P_MANA_MAX}\t\n"
          f"Enemy: \t\tHealth: {b_health}/{B_HEALTH_MAX}\t Mana: {b_mana}/{B_MANA_MAX}\n"

          f"Please input the number of your next action: attack(1), heal(2), recharge(3).")
    move = input()
    """PLAYER MOVES"""
    if move == "1":
        """Player move - Attack"""
        hit = randint(1, 20)
        print(f"You strike {b_name} for {hit} damage!")
        b_health -= hit
    elif move == "2":
        "Player move - Heal self"
        if p_mana < 20:
            print("You must recharge before trying to heal.")
            continue
        healed = randint(1, 10) + randint(1, 10)
        print(f"You spend {healed} mana and heal the same amount of health!")
        p_health += healed
        p_mana -= healed
    elif move == "3":
        "Player move - Recharge mana"
        recharged = randint(1, 5) + randint(1, 5) + randint(1, 5)
        print(f"You recharge {recharged} mana.")
        p_mana += recharged
    else:
        print("Please select a proper option. Write 1 or 2 or 3.")
        continue

    b_move = randint(1, 3)
    """BOSS MOVES"""
    if (b_move == 3 and b_mana < 50 and b_health > 20) or (b_mana < 20):
        """Boss move - recharge mana:
        When randomly rolled and boss has enough health or when mana is critically low,
        not allowing to heal self.
        """
        recharged = randint(1, 5) + randint(1, 5) + randint(1, 5)
        print(f"{b_name} recharges {recharged} mana.")
        b_mana += recharged
    elif ((b_move == 2 and b_health < 85) or (b_health <= 20 and p_health > 20)) and b_mana >= 20:
        """Boss move - heal self:
        When randomly rolled and health below 85,
        or the boss risks dying to one player's hit
        and has enough mana to spend.
        """
        healed = randint(1, 10) + randint(1, 10)
        print(f"{b_name} spends {healed} mana and heals the same amount of health!")
        b_health += healed
        b_mana -= healed
    else:
        """Boss move - attack:
        The default behaviour.
        """
        hit = randint(1, 19)
        print(f"{b_name} strikes you for {hit} damage!")
        p_health -= hit
else:
    if b_health <= 0:
        print(f"\n################################################################################"
              f"\nYou have defeated {b_name} and you will be forever known as the hero!"
              f"\n################################################################################\n")
    elif p_health <= 0:
        print(f"\n################################################################################"
              f"\n{b_name} has defeated you. Nothing will stop him now!"
              f"\n################################################################################\n")
    else:
        print("Something went wrong. Please try a new game.")
