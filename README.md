## My public repository

### /exercises/
Simple exercises.

### /image_upload_api/
Example of API using Google Cloud, allowing the user to upload images and retrieve links to differently sized thumbnails of the uploaded images.